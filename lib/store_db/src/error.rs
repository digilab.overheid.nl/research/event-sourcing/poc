use thiserror::Error;

type StdError = Box<dyn std::error::Error + Send + Sync>;

#[derive(Debug, Error)]
pub enum DatabaseError {
    #[error("Could not connect to db: `{0}`")]
    Connect(#[source] StdError),
    #[error("Could not create item: `{0}`")]
    Create(#[source] StdError),
    #[error("Could not get item: `{0}`")]
    Get(#[source] StdError),
    #[error("Could not remove item: `{0}`")]
    Delete(#[source] StdError),
    #[error("Could not convert item: `{0}`")]
    Convert(String),
    #[error("Other error: `{0}`")]
    Other(StdError),
}
