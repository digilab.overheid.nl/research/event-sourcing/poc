use async_trait::async_trait;
use store_events::{Event, EventQuery};
use uuid::Uuid;

use crate::DatabaseError;

#[async_trait]
pub trait DatabaseTrait {
    async fn build_db(database_url: &str) -> Result<DatabaseImplementation, DatabaseError>
    where
        Self: Sized;

    async fn insert(&self, event: Event) -> Result<(), DatabaseError>;
    async fn delete(&self, event_id: Uuid) -> Result<bool, DatabaseError>;
    async fn clear(&self) -> Result<bool, DatabaseError>;
    async fn get_events(&self, query: EventQuery) -> Result<Vec<Event>, DatabaseError>;
}

pub type DatabaseImplementation = Box<dyn DatabaseTrait + Send + Sync>;
