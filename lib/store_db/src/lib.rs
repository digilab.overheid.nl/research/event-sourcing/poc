mod db;
mod error;

pub use db::{DatabaseImplementation, DatabaseTrait};
pub use error::DatabaseError;
