use chrono::{DateTime, Utc};
use uuid::Uuid;

pub struct EventQuery {
    pub event_ids: Option<Vec<Uuid>>,
    pub object_ids: Option<Vec<Uuid>>,
    pub event_types: Option<Vec<String>>,
    pub start_date: Option<DateTime<Utc>>,
    pub end_date: Option<DateTime<Utc>>,
    pub event_version: Option<String>,
    pub event_kind: Option<String>,
    pub event_source_id: Option<Uuid>,
    pub event_source_type: Option<String>,
}
