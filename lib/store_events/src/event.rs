use chrono::Utc;
use serde::Serialize;
use serde_json::Value;
use uuid::Uuid;

use crate::serialize;

#[derive(Debug, Clone, Serialize)]
pub struct Event {
    pub id: Uuid,
    pub created_at: chrono::DateTime<Utc>,
    pub occurred_at: chrono::DateTime<Utc>,
    #[serde(serialize_with = "serialize::uuid_vec")]
    pub object_ids: Vec<Uuid>,
    pub event_type: String,
    pub event_version: String,
    pub event_kind: String,
    pub event_source_id: Uuid,
    pub event_source_type: String,
    pub event_reference: Vec<Uuid>,
    pub event_data: Value,
}

/// Getters
impl Event {
    pub fn id(&self) -> Uuid {
        self.id
    }
    pub fn created_at(&self) -> chrono::DateTime<Utc> {
        self.created_at
    }
    pub fn occurred_at(&self) -> chrono::DateTime<Utc> {
        self.occurred_at
    }
    pub fn object_ids(&self) -> &[Uuid] {
        &self.object_ids
    }
    pub fn event_type(&self) -> &str {
        &self.event_type
    }
    pub fn event_version(&self) -> &str {
        &self.event_version
    }
    pub fn event_kind(&self) -> &str {
        &self.event_kind
    }
    pub fn event_source_id(&self) -> Uuid {
        self.event_source_id
    }
    pub fn event_source_type(&self) -> &str {
        &self.event_source_type
    }
    pub fn event_reference(&self) -> &[Uuid] {
        &self.event_reference
    }
    pub fn event_data(&self) -> &Value {
        &self.event_data
    }
}
