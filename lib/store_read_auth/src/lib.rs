use once_cell::sync::OnceCell;
use std::collections::HashMap;
use thiserror::Error;

type SessionId = String;

#[derive(Debug, Clone)]
pub struct ReadModelSession {
    pub read_model_id: String,
    pub read_model_name: String,
}

static READ_MODEL_DB: OnceCell<ReadModelDb> = OnceCell::new();

pub struct ReadModelDb {
    db: HashMap<SessionId, ReadModelSession>,
}

pub fn init_db() -> Result<&'static ReadModelDb, DatabaseError> {
    let db = READ_MODEL_DB.get_or_init(|| {
        let mut db = HashMap::new();
        db.insert(
            "b789c080-2be1-4f2e-b051-fd2be7dab255".to_string(),
            ReadModelSession {
                read_model_id: "c41e92c1-ef3c-4cb2-98cf-d3bb50d5ec29".into(),
                read_model_name: "Persons".into(),
            },
        );
        db.insert(
            "66969bd6-a86f-424e-8cdf-d7a1bb8cf389".to_string(),
            ReadModelSession {
                read_model_id: "d74fd0ac-7b13-447b-b283-680bafb122f8".into(),
                read_model_name: "Adress".into(),
            },
        );
        ReadModelDb { db }
    });

    Ok(db)
}

impl ReadModelDb {
    pub fn get_session(&self, key: &str) -> Result<ReadModelSession, DatabaseError> {
        let session = self.db.get(key);
        session
            .cloned()
            .ok_or(DatabaseError::NotFound(key.to_string()))
    }
}

type StdError = Box<dyn std::error::Error + Send + Sync>;

#[derive(Debug, Error)]
pub enum DatabaseError {
    #[error("Could not connect to db: `{0}`")]
    Connect(#[source] StdError),
    #[error("Could not find to session: `{0}`")]
    NotFound(String),
}
