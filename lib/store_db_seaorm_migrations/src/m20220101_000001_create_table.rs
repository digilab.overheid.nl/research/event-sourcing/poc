use sea_orm_migration::prelude::*;
#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // let mut event_kind_type = Type::create();
        // event_kind_type.as_enum(EventKind::Table).values([
        //     EventKind::Claim,
        //     EventKind::CorrectionClaim,
        //     EventKind::Verification,
        // ]);
        // manager.create_type(event_kind_type).await?;

        // let mut event_source_type = Type::create();
        // event_source_type.as_enum(EventSourceType::Table).values([
        //     EventSourceType::Burger,
        //     EventSourceType::AmbtenaarBurgerlijkeStand,
        //     EventSourceType::AmbtenaarInd,
        // ]);
        // manager.create_type(event_source_type).await?;

        manager
            .create_table(
                Table::create()
                    .table(Event::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(Event::Id).uuid().not_null().primary_key())
                    .col(ColumnDef::new(Event::CreatedAt).timestamp().not_null())
                    .col(ColumnDef::new(Event::OccurredAt).timestamp().not_null())
                    .col(
                        ColumnDef::new(Event::ObjectIds)
                            .array(ColumnType::Uuid)
                            .not_null(),
                    )
                    .col(ColumnDef::new(Event::EventType).string().not_null())
                    .col(ColumnDef::new(Event::EventVersion).string().not_null())
                    .col(ColumnDef::new(Event::EventKind).string().not_null())
                    .col(ColumnDef::new(Event::EventSourceId).uuid().not_null())
                    .col(ColumnDef::new(Event::EventSourceType).string().not_null())
                    .col(
                        ColumnDef::new(Event::EventReference)
                            .array(ColumnType::Uuid)
                            .not_null(),
                    )
                    .col(ColumnDef::new(Event::EventData).json().not_null())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Event::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
pub enum Event {
    Table,
    Id,
    CreatedAt,
    OccurredAt,
    ObjectIds,
    EventType,
    EventVersion,
    EventKind,
    EventSourceId,
    EventSourceType,
    EventReference,
    EventData,
}

// #[derive(Iden, EnumIter, DeriveActiveEnum)]
// #[sea_orm(rs_type = "String", db_type = "String(None)")]
// pub enum EventKind {
//     #[sea_orm(string_value = "table")]
//     Table,
//     #[sea_orm(string_value = "claim")]
//     Claim,
//     #[sea_orm(string_value = "correction_claim")]
//     CorrectionClaim,
//     #[sea_orm(string_value = "verification")]
//     Verification,
// }

// #[derive(Iden, EnumIter, DeriveActiveEnum)]
// #[sea_orm(rs_type = "String", db_type = "String(None)")]
// pub enum EventSourceType {
//     #[sea_orm(string_value = "table")]
//     Table,
//     #[sea_orm(string_value = "claim")]
//     Burger,
//     #[sea_orm(string_value = "ambtenaar_burgerlijke_stand")]
//     AmbtenaarBurgerlijkeStand,
//     #[sea_orm(string_value = "ambtenaar_ind")]
//     AmbtenaarInd,
// }
