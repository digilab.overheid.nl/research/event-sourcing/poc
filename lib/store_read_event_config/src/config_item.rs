use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct EventPermissions {}

type EventType = String;

#[derive(Deserialize, Debug)]

pub struct EventProperties {
    pub properties: HashMap<String, Vec<String>>,
}

#[derive(Deserialize, Debug)]
pub struct EventTypeItem {
    pub events: HashMap<EventType, EventProperties>,
}
