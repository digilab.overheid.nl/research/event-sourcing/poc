use std::collections::HashMap;

use serde::Deserialize;

use crate::{EventTypeError, EventTypeItem};

type ReadModelId = String;

#[derive(Debug, Deserialize)]
pub struct EventReadConfig {
    pub read_models: HashMap<ReadModelId, EventTypeItem>,
}

impl EventReadConfig {
    pub fn validate(
        &self,
        _read_model_id: &ReadModelId,
        _event_kind: &str,
    ) -> Result<(), EventTypeError> {
        // print!("TODO: implement query validation");
        Ok(())
    }
}

// fn get_canonical_event_type(event: &Event) -> String {
//     format!("{}@{}", event.event_type(), event.event_version())
// }
