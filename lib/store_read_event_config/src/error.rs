use thiserror::Error;

// type StdError = Box<dyn std::error::Error + Send + Sync>;

#[derive(Debug, Error)]
pub enum EventTypeError {
    #[error("event type not found in configuration: `{0}`")]
    UnkownType(String),
    #[error("Not allowed")]
    NotAllowed,
}
