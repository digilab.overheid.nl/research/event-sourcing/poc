use thiserror::Error;

type StdError = Box<dyn std::error::Error + Send + Sync>;

#[derive(Debug, Error)]
pub enum EventTypeError {
    #[error("event type not found in configuration: `{0}`")]
    UnkownType(String),
    #[error("event kind not found in configuration: `{0}`")]
    UnkownKind(String),
    #[error("event source type not found in configuration: `{0}`")]
    UnkownSourceType(String),
    #[error("Not allowed")]
    NotAllowed,
    #[error("Could not parse schema: `{0}`")]
    ParseSchema(#[source] StdError),
    #[error("Schema validation failed: `{0}`")]
    SchemaValidate(String),
}
