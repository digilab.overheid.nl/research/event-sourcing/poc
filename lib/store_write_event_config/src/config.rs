use std::collections::HashMap;

use serde::Deserialize;
use store_events::Event;

use crate::{EventTypeError, EventTypeItem};

type EventName = String;

#[derive(Debug, Deserialize)]
pub struct EventWriteConfig {
    pub event_types: HashMap<EventName, EventTypeItem>,
}

impl EventWriteConfig {
    pub fn validate(&self, event: &Event) -> Result<(), EventTypeError> {
        let event_type_name = get_canonical_event_type(event);

        let config = self
            .event_types
            .get(&event_type_name)
            .ok_or_else(|| EventTypeError::UnkownType(event_type_name))?;

        let event_kind = event.event_kind();
        let event_source_type = event.event_source_type();

        let allowed = config
            .permissions
            .get(event_kind)
            .ok_or_else(|| EventTypeError::UnkownKind(event_kind.to_string()))?
            .get(event_source_type)
            .ok_or_else(|| EventTypeError::UnkownSourceType(event_source_type.to_string()))?;

        if !allowed {
            return Err(EventTypeError::NotAllowed);
        }

        let event_data = event.event_data();
        let result = config.event_data_schema.validate(event_data);
        if let Err(errors) = result {
            let error_message = errors
                .map(|e| format!("{}: {}", e.instance_path, e))
                .collect::<Vec<String>>()
                .join(",");
            return Err(EventTypeError::SchemaValidate(error_message));
        }

        Ok(())
    }
}

fn get_canonical_event_type(event: &Event) -> String {
    format!("{}@{}", event.event_type(), event.event_version())
}
