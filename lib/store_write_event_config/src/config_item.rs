use std::collections::HashMap;

use jsonschema::{Draft, JSONSchema};
use serde::{de::Error as SerdeError, Deserialize, Deserializer, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct EventPermissions {}

type EventKind = String;
type EventSourceType = String;

/// New type to allow custom deserialize
// pub struct Schema(JSONSchema);

#[derive(Deserialize)]
pub struct EventTypeItem {
    #[serde(deserialize_with = "from_schema_string")]
    pub event_data_schema: JSONSchema,
    pub permissions: HashMap<EventKind, HashMap<EventSourceType, bool>>,
}

impl std::fmt::Debug for EventTypeItem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("EventTypeItem")
            .field("event_data_schema", &"...")
            .field("permissions", &self.permissions)
            .finish()
    }
}

fn from_schema_string<'de, D>(deserializer: D) -> Result<JSONSchema, D::Error>
where
    D: Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    let json = serde_json::from_str(&s).map_err(|e| SerdeError::custom(e.to_string()))?;

    let compiled = JSONSchema::options()
        .with_draft(Draft::Draft7)
        .with_format("custom-country-code", country_code_format)
        .compile(&json)
        .expect("A valid schema");

    Ok(compiled)
}

fn country_code_format(s: &str) -> bool {
    // Your awesome format check!
    ["NL", "GB"].contains(&s)
}
