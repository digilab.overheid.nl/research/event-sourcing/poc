use async_graphql::{Context, Object, Result};

use anyhow::anyhow;
use store_db::DatabaseImplementation;
use uuid::Uuid;

#[derive(Default)]
pub struct MutationDeleteEvent;

#[Object]
impl MutationDeleteEvent {
    pub async fn delete_event(&self, ctx: &Context<'_>, event_id: Uuid) -> Result<bool> {
        let db = ctx.data::<DatabaseImplementation>().unwrap();

        let event_removed = db
            .delete(event_id)
            .await
            .map_err(|e| anyhow!("Delete failed {}", e))?;

        Ok(event_removed)
    }
}
