mod add_event;
mod clear_events;
mod delete_events;

pub use add_event::MutationAddEvent;
pub use clear_events::MutationClearEvents;
pub use delete_events::MutationDeleteEvent;

#[derive(async_graphql::MergedObject, Default)]
pub struct Mutation(MutationAddEvent, MutationDeleteEvent, MutationClearEvents);
