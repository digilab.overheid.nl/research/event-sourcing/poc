use async_graphql::{Context, Object, Result};

use anyhow::anyhow;
use store_db::DatabaseImplementation;

#[derive(Default)]
pub struct MutationClearEvents;

#[Object]
impl MutationClearEvents {
    pub async fn clear_events(&self, ctx: &Context<'_>) -> Result<bool> {
        let db = ctx.data::<DatabaseImplementation>().unwrap();

        let event_removed = db
            .clear()
            .await
            .map_err(|e| anyhow!("Clear failed {}", e))?;

        Ok(event_removed)
    }
}
