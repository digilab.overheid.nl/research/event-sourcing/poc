use std::str::FromStr;

use anyhow::anyhow;
use async_graphql::{Context, Object, Result};
use chrono::Utc;
use uuid::Uuid;

use store_db::DatabaseImplementation;
use store_events::Event;
use store_write_auth::UserSession;
use store_write_event_config::EventWriteConfig;

use store_gql::EventCreateValue;

#[derive(Default)]
pub struct MutationAddEvent;

#[Object]
impl MutationAddEvent {
    pub async fn add_event(&self, ctx: &Context<'_>, event: EventCreateValue) -> Result<Uuid> {
        let db = ctx.data::<DatabaseImplementation>()?;
        let event_write_config = ctx.data::<EventWriteConfig>()?;
        let session = ctx.data::<UserSession>()?;

        let created_at = Utc::now();

        let store_event = Event {
            id: Uuid::new_v4(),
            created_at,
            occurred_at: event.occurred_at,
            object_ids: event.object_ids,
            event_type: event.event_type,
            event_version: event.event_version,
            event_kind: event.event_kind.as_str().to_string(),
            event_source_id: Uuid::from_str(&session.user_id)?,
            event_source_type: session.user_type.clone(),
            event_reference: event.event_reference,
            event_data: event.event_data,
        };

        event_write_config.validate(&store_event)?;

        let uuid = store_event.id();
        db.insert(store_event)
            .await
            .map_err(|e| anyhow!("Insert failed {}", e))?;
        Ok(uuid)
    }
}
