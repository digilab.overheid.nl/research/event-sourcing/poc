use anyhow::anyhow;
use async_graphql::{Context, Object, Result};
use store_read_auth::ReadModelSession;
use store_read_event_config::EventReadConfig;

use store_db::DatabaseImplementation;
use store_gql::{EventQueryValue, EventValue};

#[derive(Default)]
pub struct QueryEvents;

#[Object]
impl QueryEvents {
    async fn get(&self, ctx: &Context<'_>, query: EventQueryValue) -> Result<Vec<EventValue>> {
        let db = ctx.data::<DatabaseImplementation>().unwrap();
        let event_read_config = ctx.data::<EventReadConfig>()?;
        let session = ctx.data::<ReadModelSession>()?;
        println!("Session {:?}", session);

        event_read_config.validate(&session.read_model_id, "Claim")?;

        let events = db
            .get_events(query.into())
            .await
            .map_err(|e| anyhow!("Get failed {}", e))?;

        let event_values = events
            .iter()
            .map(|v| v.try_into())
            .collect::<Result<Vec<EventValue>>>()?;

        Ok(event_values)
    }
}
