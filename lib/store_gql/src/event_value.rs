use async_graphql::{InputObject, SimpleObject};
use chrono::{DateTime, Utc};
use serde_json::Value;
use store_events::{Event, EventQuery};
use uuid::Uuid;

use super::EventKind;

#[derive(SimpleObject)]
pub struct EventValue {
    id: Uuid,
    created_at: DateTime<Utc>,
    occurred_at: DateTime<Utc>,
    object_ids: Vec<Uuid>,
    event_type: String,
    event_version: String,
    event_kind: EventKind,
    event_source_id: Uuid,
    event_source_type: String,
    event_reference: Vec<Uuid>,
    event_data: Value,
}

impl TryFrom<&Event> for EventValue {
    type Error = async_graphql::Error;

    fn try_from(value: &Event) -> Result<Self, Self::Error> {
        Ok(EventValue {
            id: value.id(),
            created_at: value.created_at(),
            occurred_at: value.occurred_at(),
            object_ids: value.object_ids().to_vec(),
            event_type: value.event_type().to_string(),
            event_version: value.event_version().to_string(),
            event_kind: value.event_kind().try_into()?,
            event_source_id: value.event_source_id(),
            event_source_type: value.event_source_type().to_string(),
            event_reference: value.event_reference().to_vec(),
            event_data: value.event_data().clone(),
        })
    }
}

#[derive(InputObject)]
pub struct EventCreateValue {
    pub occurred_at: DateTime<Utc>,
    pub object_ids: Vec<Uuid>,
    pub event_type: String,
    pub event_version: String,
    pub event_kind: EventKind,
    pub event_reference: Vec<Uuid>,
    pub event_data: Value,
}

#[derive(InputObject)]
pub struct EventQueryValue {
    pub event_ids: Option<Vec<Uuid>>,
    pub object_ids: Option<Vec<Uuid>>,
    pub event_types: Option<Vec<String>>,
    pub start_date: Option<DateTime<Utc>>,
    pub end_date: Option<DateTime<Utc>>,
    pub event_version: Option<String>,
    pub event_kind: Option<EventKind>,
    pub event_source_id: Option<Uuid>,
    pub event_source_type: Option<String>,
}

impl From<EventQueryValue> for EventQuery {
    fn from(value: EventQueryValue) -> Self {
        EventQuery {
            event_ids: value.event_ids,
            object_ids: value.object_ids,
            event_types: value.event_types,
            start_date: value.start_date,
            end_date: value.end_date,
            event_version: value.event_version,
            event_kind: value.event_kind.map(|v| v.as_str().to_string()),
            event_source_id: value.event_source_id,
            event_source_type: value.event_source_type,
        }
    }
}
