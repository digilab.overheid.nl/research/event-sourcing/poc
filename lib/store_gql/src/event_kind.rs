use async_graphql::*;

#[derive(Enum, Copy, Clone, Eq, PartialEq)]
pub enum EventKind {
    Claim,
    CorrectionClaim,
    Verification,
}

impl EventKind {
    pub fn as_str(&self) -> &str {
        match self {
            EventKind::Claim => "Claim",
            EventKind::CorrectionClaim => "CorrectionClaim",
            EventKind::Verification => "Verification",
        }
    }
}

impl TryFrom<&str> for EventKind {
    type Error = async_graphql::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "Claim" => Ok(EventKind::Claim),
            "CorrectionClaim" => Ok(EventKind::CorrectionClaim),
            "Verification" => Ok(EventKind::Verification),
            v => Err(async_graphql::Error::new(format!(
                "Unknown EventKind {}",
                v
            ))),
        }
    }
}
