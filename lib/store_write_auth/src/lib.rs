use once_cell::sync::OnceCell;
use std::collections::HashMap;
use thiserror::Error;

#[derive(Debug, Clone)]
pub struct UserSession {
    pub user_id: String,
    pub user_name: String,
    pub user_type: String,
}

static USER_DB: OnceCell<UserDb> = OnceCell::new();

pub struct UserDb {
    db: HashMap<String, UserSession>,
}

pub fn init_db() -> Result<&'static UserDb, DatabaseError> {
    let db = USER_DB.get_or_init(|| {
        let mut db = HashMap::new();
        db.insert(
            "94872a06-611f-4696-9bfd-1a55d0752bc2".to_string(),
            UserSession {
                user_id: "922d12fb-9890-480d-b14b-014218ac1aa5".into(),
                user_name: "Henk Jansen".into(),
                user_type: "AmbtenaarBurgerlijkeStand".into(),
            },
        );
        db.insert(
            "49672b58-11d7-4cc8-9cd0-336f3ad9a182".to_string(),
            UserSession {
                user_id: "317956fc-6b7b-4fc5-b576-d86e2454ba45".into(),
                user_name: "Frank Brasem".into(),
                user_type: "Burger".into(),
            },
        );
        db.insert(
            "94872a06-611f-4696-9bfd-1a55d0752bc2".to_string(),
            UserSession {
                user_id: "922d12fb-9890-480d-b14b-014218ac1aa5".into(),
                user_name: "Joop van der Schulp".into(),
                user_type: "AmbtenaarInd".into(),
            },
        );

        UserDb { db }
    });

    Ok(db)
}

impl UserDb {
    pub fn get_session(&self, key: &str) -> Result<UserSession, DatabaseError> {
        let session = self.db.get(key);
        session
            .cloned()
            .ok_or(DatabaseError::NotFound(key.to_string()))
    }
}

type StdError = Box<dyn std::error::Error + Send + Sync>;

#[derive(Debug, Error)]
pub enum DatabaseError {
    #[error("Could not connect to db: `{0}`")]
    Connect(#[source] StdError),
    #[error("Could not find to session: `{0}`")]
    NotFound(String),
}
