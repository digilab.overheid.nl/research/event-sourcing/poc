use async_trait::async_trait;
use sea_orm::{
    prelude::*, ActiveModelTrait, Database, DatabaseConnection, EntityTrait, QueryTrait, Set,
};

use store_db::{DatabaseError, DatabaseImplementation, DatabaseTrait};
use store_db_seaorm_migrations::{Expr, Migrator, MigratorTrait, SimpleExpr};
use store_events::{Event as StoreEvent, EventQuery};

use crate::schema::{
    event::{ActiveModel as ActiveEventModel, Column as EventColumn, Model as EventModel},
    prelude::Event,
};

pub struct SeaOrmDb {
    client: DatabaseConnection,
}

impl SeaOrmDb {
    pub async fn run_migrations(database_url: &str) -> Result<(), DatabaseError> {
        let connection = sea_orm::Database::connect(database_url)
            .await
            .map_err(|e| DatabaseError::Connect(Box::new(e)))?;

        Migrator::up(&connection, None)
            .await
            .map_err(|e| DatabaseError::Other(Box::new(e)))?;
        Ok(())
    }
}

#[async_trait]
impl DatabaseTrait for SeaOrmDb {
    async fn build_db(database_url: &str) -> Result<DatabaseImplementation, DatabaseError> {
        let client = Database::connect(database_url)
            .await
            .map_err(|e| DatabaseError::Connect(Box::new(e)))?;

        return Ok(Box::new(SeaOrmDb { client }));
    }

    async fn insert(&self, event: StoreEvent) -> Result<(), DatabaseError> {
        let event = ActiveEventModel {
            id: Set(event.id()),
            created_at: Set(event.created_at().naive_utc()),
            occurred_at: Set(event.occurred_at().naive_utc()),
            object_ids: Set(event.object_ids().to_vec()),
            event_type: Set(event.event_type().to_string()),
            event_version: Set(event.event_version().to_string()),
            event_kind: Set(event.event_kind().to_string()),
            event_source_id: Set(event.event_source_id()),
            event_source_type: Set(event.event_source_type().to_string()),
            event_reference: Set(event.event_reference().to_vec()),
            event_data: Set(event.event_data().clone()),
        };
        event
            .insert(&self.client)
            .await
            .map_err(|e| DatabaseError::Create(Box::new(e)))?;
        Ok(())
    }

    async fn delete(&self, event_id: Uuid) -> Result<bool, DatabaseError> {
        let event = ActiveEventModel {
            id: Set(event_id),
            ..Default::default()
        };

        let rows_affected = Event::delete(event)
            .exec(&self.client)
            .await
            .map_err(|e| DatabaseError::Delete(Box::new(e)))?
            .rows_affected;

        Ok(rows_affected > 0)
    }

    async fn clear(&self) -> Result<bool, DatabaseError> {
        let rows_affected = Event::delete_many()
            .exec(&self.client)
            .await
            .map_err(|e| DatabaseError::Delete(Box::new(e)))?
            .rows_affected;

        Ok(rows_affected > 0)
    }

    async fn get_events(&self, event_query: EventQuery) -> Result<Vec<StoreEvent>, DatabaseError> {
        let query = Event::find();

        let query = query
            .apply_if(event_query.object_ids, |query, object_ids| {
                // TODO: Code is currently pretty hacky with the CustomWithExpr, but digging into the orm doesn't seem worth it at the moment
                query.filter(SimpleExpr::CustomWithExpr(
                    format!(
                        "$1 && '{{{}}}'",
                        object_ids
                            .iter()
                            .map(|et| et.to_string())
                            .collect::<Vec<String>>()
                            .join(","),
                    ),
                    vec![SimpleExpr::from(Expr::col(EventColumn::ObjectIds))],
                ))
            })
            .apply_if(event_query.event_ids, |query, event_ids| {
                query.filter(Expr::col(EventColumn::Id).is_in(event_ids))
            })
            .apply_if(event_query.event_types, |query, event_types| {
                query.filter(Expr::col(EventColumn::EventType).is_in(event_types))
            })
            .apply_if(event_query.start_date, |query, start_date| {
                query.filter(Expr::col(EventColumn::OccurredAt).gte(start_date))
            })
            .apply_if(event_query.end_date, |query, end_date| {
                query.filter(Expr::col(EventColumn::OccurredAt).lte(end_date))
            })
            .apply_if(event_query.event_version, |query, event_version| {
                query.filter(Expr::col(EventColumn::EventVersion).eq(event_version))
            })
            .apply_if(event_query.event_kind, |query, event_kind| {
                query.filter(Expr::col(EventColumn::EventKind).eq(event_kind))
            })
            .apply_if(event_query.event_source_id, |query, event_source_id| {
                query.filter(Expr::col(EventColumn::EventSourceId).eq(event_source_id))
            })
            .apply_if(event_query.event_source_type, |query, event_source_type| {
                query.filter(Expr::col(EventColumn::EventSourceType).eq(event_source_type))
            });

        let db_events = query
            .all(&self.client)
            .await
            .map_err(|e| DatabaseError::Get(Box::new(e)))?;

        let events = db_events
            .into_iter()
            .map(StoreEvent::try_from)
            .collect::<Result<Vec<StoreEvent>, _>>()
            .map_err(|_| DatabaseError::Convert("Database events".into()))?;

        Ok(events)
    }
}

type StdError = Box<dyn std::error::Error>;

impl TryFrom<EventModel> for StoreEvent {
    type Error = StdError;

    fn try_from(value: EventModel) -> Result<Self, Self::Error> {
        Ok(Self {
            id: value.id,
            created_at: value.created_at.and_utc(),
            occurred_at: value.occurred_at.and_utc(),
            object_ids: value.object_ids,
            event_type: value.event_type,
            event_version: value.event_version,
            event_kind: value.event_kind,
            event_source_id: value.event_source_id,
            event_source_type: value.event_source_type,
            event_reference: value.event_reference,
            event_data: value.event_data,
        })
    }
}
