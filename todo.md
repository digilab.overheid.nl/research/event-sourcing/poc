# Open
- data generator
- event read/query permissions

# Done
- filtering on new properties
- event write/read split
- Rbac / abac
    - custom bouwen
- cleanup todo's
- event version:
    - v0.0.1
- event kind: claim, verify/ack, correction
- event source: ambtenaar A (ambtenaar burgelijke stand)
    - claimant
        - id
        - type (BURGER | AMBTENAAR_BS | AMBTENAAR_IND)
- event reference: verwijs terug naar vorig event
    - event_ids