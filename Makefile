.PHONY: write
write:
	cargo run -p store_write_server

.PHONY: read
read:
	cargo run -p store_read_server

.PHONY: generate
generate:
	cargo run -p event_generator


# cargo install cargo-watch
.PHONY: watch
watch:
	cargo watch -x 'run -p store_write_server'

.PHONY: watch-read
watch-read:
	cargo watch -x 'run -p store_read_server'

.PHONY: docker_build
docker_build: setup
	docker build -f ./tools/scripts/Dockerfile --build-arg APP_BINARY_NAME=store_write_server .
	docker build -f ./tools/scripts/Dockerfile --build-arg APP_BINARY_NAME=store_read_server .

.PHONY: check
check: udeps clippy

.PHONY: setup
setup: postgresql seaorm_migrate seaorm_gen

# Tooling

## cargo install cargo-udeps
.PHONY: udeps
udeps:
	cargo +nightly udeps

.PHONY: clippy
clippy:
	cargo clippy

## DB
.PHONY: postgresql
postgresql:
	docker-compose --file tools/scripts/docker-compose.yml up --detach

## SeaOrm
.PHONY: seaorm_migrate
seaorm_migrate:
	DATABASE_URL='postgresql://postgres:postgres@localhost:5432/events' \
		sea-orm-cli migrate up \
		--migration-dir ./tools/bin/seaorm_migrate
	
.PHONY: seaorm_gen
seaorm_gen:
	DATABASE_URL='postgresql://postgres:postgres@localhost:5432/events' \
		sea-orm-cli generate entity -o ./lib/store_db_seaorm/src/schema