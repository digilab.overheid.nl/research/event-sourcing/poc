use anyhow::Result;
use sea_orm_migration::{
    prelude::*,
    sea_orm::{Database, Statement},
};

use store_db_seaorm_migrations::Migrator;

#[tokio::main]
async fn main() -> Result<()> {
    let db_url = std::env::var("DATABASE_URL").unwrap();
    let db_name = "events";

    let base_url = db_url.trim_end_matches(&format!("/{}", db_name));

    let db = Database::connect(base_url).await.unwrap();

    println!("Dropping database");
    db.execute(Statement::from_string(
        db.get_database_backend(),
        format!("DROP DATABASE IF EXISTS \"{}\";", db_name),
    ))
    .await?;

    println!("Creating database");
    let res = db
        .execute(Statement::from_string(
            db.get_database_backend(),
            format!("CREATE DATABASE \"{db_name}\";"),
        ))
        .await;
    if let Err(e) = res {
        if e.to_string()
            != format!(
                "Execution Error: error returned from database: database \"{}\" already exists",
                db_name
            )
        {
            panic!("Database creation failed")
        }
    }

    println!("Running migrations");
    cli::run_cli(Migrator).await;

    Ok(())
}
