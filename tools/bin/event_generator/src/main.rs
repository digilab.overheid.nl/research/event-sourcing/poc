use std::{fs::File, io::BufReader};

use anyhow::{anyhow, Result};
use clap::Parser;
use csv::ReaderBuilder;
use reqwest::{Client, StatusCode};
use serde::Serialize;

mod gql;
mod record;

use record::EventRecord;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Endpoint to connect to e.g.
    #[arg(long, default_value = "http://localhost:3000/write")]
    api_url: String,
    /// Path to load event csv from
    #[arg(long, default_value = "./docs/event_generator_input.csv")]
    event_csv_path: String,
    /// "Authorization" header to send with requests
    #[arg(long, default_value = "Bearer 94872a06-611f-4696-9bfd-1a55d0752bc2")]
    authorization_header: String,
}

struct ApiConfig {
    url: String,
    auth_header: String,
}

#[tokio::main(worker_threads = 12)]
async fn main() -> Result<()> {
    let args = Args::parse();

    println!("Start generator");
    let client = Client::new();
    let api_config = ApiConfig {
        url: args.api_url,
        auth_header: args.authorization_header,
    };

    println!("Read csv input");
    let f = File::open(args.event_csv_path)?;
    let reader = BufReader::new(f);
    let mut rdr = ReaderBuilder::new().delimiter(b',').from_reader(reader);

    for result in rdr.deserialize() {
        let record: EventRecord = result?;

        println!(
            "Send request {:?} {:.8} - `{:.10}` `{:.10}` `{:.10}` `{:.10}`",
            record.event_type,
            record.user_id.to_string(),
            record.data1,
            record.data2,
            record.data3,
            record.data4
        );

        match record.event_type {
            record::EventType::Clear => send_req(&client, &api_config, &gql::clear::build()).await,
            record::EventType::AddGeboorte => {
                send_req(&client, &api_config, &gql::add_geboorte::build(record)?).await
            }
            record::EventType::AddName => {
                send_req(&client, &api_config, &gql::add_name::build(record)).await
            }
            record::EventType::AddVerhuizing => {
                send_req(&client, &api_config, &gql::add_verhuizing::build(record)?).await
            }
            record::EventType::AddOverleiden => {
                send_req(&client, &api_config, &gql::add_overleiden::build(record)?).await
            }
        }?;
    }

    println!("Done");
    Ok(())
}

async fn send_req<T>(client: &Client, config: &ApiConfig, body: &T) -> Result<()>
where
    T: Serialize + ?Sized,
{
    let res = client
        .post(&config.url)
        .header("Authorization", &config.auth_header)
        .json(&body)
        .send()
        .await
        .unwrap();

    if res.status() != StatusCode::OK {
        return Err(anyhow!("REQUEST FAILED: {}", res.status()));
    } else {
        println!("\tsent successfully");
    }

    Ok(())
}
