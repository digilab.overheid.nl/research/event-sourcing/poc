use chrono::Utc;

#[derive(Debug, serde::Deserialize)]
pub struct EventRecord {
    pub occurred_at: chrono::DateTime<Utc>,
    pub user_id: uuid::Uuid,
    pub event_type: EventType,
    pub event_version: String,
    pub event_kind: EventKind,
    pub data1: String,
    pub data2: String,
    pub data3: String,
    pub data4: String,
}

#[derive(Debug, serde::Deserialize)]
pub enum EventType {
    Clear,
    AddGeboorte,
    AddName,
    AddVerhuizing,
    AddOverleiden,
}

#[derive(Debug, serde::Deserialize)]
pub enum EventKind {
    Claim,
    Correction,
    Validation,
}
