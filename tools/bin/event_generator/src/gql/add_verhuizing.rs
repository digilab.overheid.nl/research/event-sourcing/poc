use std::str::FromStr;

use add_verhuizing::Variables;
use anyhow::Result;
use graphql_client::{GraphQLQuery, QueryBody};

use crate::record::{EventKind, EventRecord};

use super::common::*;

// The paths are relative to the directory where your `Cargo.toml` is located.
// Both json and the GraphQL schema language are supported as sources for the schema
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/gql/schema.gql",
    mutation_path = "src/gql/add_verhuizing.gql",
    query_path = "src/gql/add_verhuizing.gql",
    response_derives = "Debug"
)]
pub struct AddVerhuizing;

pub fn build(record: EventRecord) -> Result<QueryBody<Variables>> {
    let variables = add_verhuizing::Variables {
        occurred_at: record.occurred_at,
        user_id: record.user_id,
        event_version: record.event_version,
        event_kind: record.event_kind.into(),
        datum_aanvang_adreshouding: DateTime::from_str(&record.data1)?,
        datum_ingang_adreshouding: DateTime::from_str(&record.data2)?,
        type_adres: record.data3,
        bag_adres_referentie: record.data4,
    };

    Ok(AddVerhuizing::build_query(variables))
}

impl From<EventKind> for add_verhuizing::EventKind {
    fn from(value: EventKind) -> Self {
        match value {
            EventKind::Claim => add_verhuizing::EventKind::CLAIM,
            EventKind::Correction => add_verhuizing::EventKind::CORRECTION_CLAIM,
            EventKind::Validation => add_verhuizing::EventKind::VERIFICATION,
        }
    }
}
