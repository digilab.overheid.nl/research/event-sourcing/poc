use add_name::Variables;
use graphql_client::{GraphQLQuery, QueryBody};

use crate::record::{EventKind, EventRecord};

use super::common::*;

// The paths are relative to the directory where your `Cargo.toml` is located.
// Both json and the GraphQL schema language are supported as sources for the schema
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/gql/schema.gql",
    mutation_path = "src/gql/add_name.gql",
    query_path = "src/gql/add_name.gql",
    response_derives = "Debug"
)]
pub struct AddName;

pub fn build(record: EventRecord) -> QueryBody<Variables> {
    let variables = add_name::Variables {
        occurred_at: record.occurred_at,
        user_id: record.user_id,
        event_version: record.event_version,
        event_kind: record.event_kind.into(),
        voornamen: record.data1,
        voorvoegsel: record.data2,
        geslachtsnaam: record.data3,
    };

    AddName::build_query(variables)
}

impl From<EventKind> for add_name::EventKind {
    fn from(value: EventKind) -> Self {
        match value {
            EventKind::Claim => add_name::EventKind::CLAIM,
            EventKind::Correction => add_name::EventKind::CORRECTION_CLAIM,
            EventKind::Validation => add_name::EventKind::VERIFICATION,
        }
    }
}
