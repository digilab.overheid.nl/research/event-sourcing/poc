use std::str::FromStr;

use add_overleiden::Variables;
use anyhow::Result;
use graphql_client::{GraphQLQuery, QueryBody};

use crate::record::{EventKind, EventRecord};

use super::common::*;

// The paths are relative to the directory where your `Cargo.toml` is located.
// Both json and the GraphQL schema language are supported as sources for the schema
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/gql/schema.gql",
    mutation_path = "src/gql/add_overleiden.gql",
    query_path = "src/gql/add_overleiden.gql",
    response_derives = "Debug"
)]
pub struct AddOverleiden;

pub fn build(record: EventRecord) -> Result<QueryBody<Variables>> {
    let variables = add_overleiden::Variables {
        occurred_at: record.occurred_at,
        user_id: record.user_id,
        event_version: record.event_version,
        event_kind: record.event_kind.into(),
        overlijdensdatum: DateTime::from_str(&record.data1)?,
        overlijdensland: record.data2,
        overlijdensgebied: record.data3,
        overlijdensplaats: record.data4,
    };

    Ok(AddOverleiden::build_query(variables))
}

impl From<EventKind> for add_overleiden::EventKind {
    fn from(value: EventKind) -> Self {
        match value {
            EventKind::Claim => add_overleiden::EventKind::CLAIM,
            EventKind::Correction => add_overleiden::EventKind::CORRECTION_CLAIM,
            EventKind::Validation => add_overleiden::EventKind::VERIFICATION,
        }
    }
}
