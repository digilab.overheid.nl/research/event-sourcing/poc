use chrono::Utc;
use uuid::Uuid;

pub type DateTime = chrono::DateTime<Utc>;
#[allow(clippy::upper_case_acronyms)]
pub type UUID = Uuid;
