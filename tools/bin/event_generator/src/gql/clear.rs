use clear_events::Variables;
use graphql_client::{GraphQLQuery, QueryBody};

// The paths are relative to the directory where your `Cargo.toml` is located.
// Both json and the GraphQL schema language are supported as sources for the schema
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/gql/schema.gql",
    mutation_path = "src/gql/clear_events.gql",
    query_path = "src/gql/clear_events.gql",
    response_derives = "Debug"
)]
pub struct ClearEvents;

pub fn build() -> QueryBody<Variables> {
    let variables = clear_events::Variables {};

    ClearEvents::build_query(variables)
}
