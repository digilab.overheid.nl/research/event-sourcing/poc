use std::str::FromStr;

use anyhow::Result;
use graphql_client::{GraphQLQuery, QueryBody};

use crate::record::{EventKind, EventRecord};

use super::common::*;
use add_geboorte::Variables;

// The paths are relative to the directory where your `Cargo.toml` is located.
// Both json and the GraphQL schema language are supported as sources for the schema
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/gql/schema.gql",
    mutation_path = "src/gql/add_geboorte.gql",
    query_path = "src/gql/add_geboorte.gql",
    response_derives = "Debug"
)]
pub struct AddGeboorte;

pub fn build(record: EventRecord) -> Result<QueryBody<Variables>> {
    let variables = add_geboorte::Variables {
        occurred_at: record.occurred_at,
        user_id: record.user_id,
        event_version: record.event_version,
        event_kind: record.event_kind.into(),
        geboortedatum: DateTime::from_str(&record.data1)?,
        geboorteland: record.data2,
        geboortegebied: record.data3,
        geboorteplaats: record.data4,
    };

    Ok(AddGeboorte::build_query(variables))
}

impl From<EventKind> for add_geboorte::EventKind {
    fn from(value: EventKind) -> Self {
        match value {
            EventKind::Claim => add_geboorte::EventKind::CLAIM,
            EventKind::Correction => add_geboorte::EventKind::CORRECTION_CLAIM,
            EventKind::Validation => add_geboorte::EventKind::VERIFICATION,
        }
    }
}
