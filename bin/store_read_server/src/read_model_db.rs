use anyhow::Result;

use store_read_auth::ReadModelDb;

pub async fn setup() -> Result<&'static ReadModelDb> {
    // Run migrations to setup database
    let db = store_read_auth::init_db()?;

    Ok(db)
}
