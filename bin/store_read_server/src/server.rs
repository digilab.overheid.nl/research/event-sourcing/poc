use anyhow::Result;
use async_graphql::http::{playground_source, GraphQLPlaygroundConfig};
use async_graphql_axum::{GraphQLRequest, GraphQLResponse};
use axum::{
    extract::Extension,
    response::{Html, IntoResponse},
    routing::get,
    Router,
};
use store_read_auth::ReadModelDb;

use crate::{middleware::AuthenticatedModel, schema::AppSchema};

pub async fn serve(schema: AppSchema, read_model_db: &'static ReadModelDb) -> Result<()> {
    let app = Router::new()
        .route(
            "/read",
            get(graphql_playground).post(graphql_handler),
        )
        .route("/healthz", get(liveness_handler))
        .layer(Extension(schema))
        .layer(Extension(read_model_db));

    println!("Playground: http://localhost:3001/read");

    axum::Server::bind(&"0.0.0.0:3001".parse()?)
        .serve(app.into_make_service())
        .await?;
    Ok(())
}

async fn graphql_playground() -> impl IntoResponse {
    Html(playground_source(GraphQLPlaygroundConfig::new(
        "/read",
    )))
}

async fn graphql_handler(
    user: AuthenticatedModel,
    schema: Extension<AppSchema>,
    req: GraphQLRequest,
) -> GraphQLResponse {
    schema
        .execute(req.into_inner().data(user.into_inner()))
        .await
        .into()
}

async fn liveness_handler() -> String {
    "OK".to_string()
}
