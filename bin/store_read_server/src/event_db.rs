use anyhow::{anyhow, Result};

use store_db::{DatabaseImplementation, DatabaseTrait};

use store_db_seaorm::SeaOrmDb;

pub async fn setup(database_url: &str) -> Result<DatabaseImplementation> {
    let db = SeaOrmDb::build_db(database_url)
        .await
        .map_err(|e| anyhow!("Could not connect to db: `{}`", e))?;
    Ok(db)
}
