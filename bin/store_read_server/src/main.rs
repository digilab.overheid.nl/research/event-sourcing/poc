use anyhow::Result;
use clap::Parser;
use store_read_event_config::EventReadConfig;

mod event_db;
mod middleware;
mod read_model_db;
mod schema;
mod server;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Database url to connect to e.g. `postgresql://postgres:postgres@localhost:5432/events`
    #[arg(
        long,
        default_value = "postgresql://postgres:postgres@localhost:5432/events"
    )]
    database_url: String,
    /// Path to load event write configuration from `./docs/event_read_config_example.yaml`
    #[arg(long, default_value = "./docs/event_read_config_example.yaml")]
    read_config_path: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();
    let database_url = load_config_value(args.database_url, "DATABASE_URL");

    let read_config_path = load_config_value(args.read_config_path, "READ_CONFIG_PATH");
    let read_config_string = std::fs::read_to_string(read_config_path)?;
    let read_config: EventReadConfig = serde_yaml::from_str(&read_config_string)?;

    println!(
        "Connecting to database: `{}`",
        &database_url.split('@').last().unwrap_or_default()
    );
    println!("Event write config: `{:?}`", read_config);

    let event_db = event_db::setup(&database_url).await?;
    let schema = schema::build(event_db, read_config).await;

    let user_db = read_model_db::setup().await?;

    server::serve(schema, user_db).await?;
    Ok(())
}

fn load_config_value(arg: String, key: &str) -> String {
    let env_value = std::env::var(key.to_uppercase());
    if let Ok(env_value) = env_value {
        env_value
    } else {
        arg
    }
}
