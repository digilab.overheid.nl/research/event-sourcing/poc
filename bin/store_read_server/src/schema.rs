use async_graphql::{EmptyMutation, EmptySubscription, Schema};

use store_db::DatabaseImplementation;
use store_read_event_config::EventReadConfig;
use store_read_gql::Query;

pub type AppSchema = Schema<Query, EmptyMutation, EmptySubscription>;

pub async fn build(db: DatabaseImplementation, event_write_config: EventReadConfig) -> AppSchema {
    Schema::build(Query::default(), EmptyMutation, EmptySubscription)
        .data(db)
        .data(event_write_config)
        .finish()
}
