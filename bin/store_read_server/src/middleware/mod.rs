use async_trait::async_trait;
use axum::{
    extract::FromRequestParts,
    headers::{authorization::Bearer, Authorization},
    http::{request::Parts, StatusCode},
    Extension, TypedHeader,
};

use store_read_auth::{ReadModelDb, ReadModelSession};

pub struct AuthenticatedModel(ReadModelSession);

#[async_trait]
impl<S> FromRequestParts<S> for AuthenticatedModel
where
    S: Send + Sync,
{
    type Rejection = StatusCode;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let token = TypedHeader::<Authorization<Bearer>>::from_request_parts(parts, state)
            .await
            .map(|token| token.token().to_string())
            .unwrap_or_default();

        use axum::RequestPartsExt;

        let Extension(state) = parts
            .extract::<Extension<&'static ReadModelDb>>()
            .await
            .map_err(|err| {
                println!(
                    "FromRequest Authenticated read_model extension rejection {}",
                    err
                );
                StatusCode::INTERNAL_SERVER_ERROR
            })?;

        if token.is_empty() {
            println!("FromRequest Authenticated empty auth bearer");
            Err(StatusCode::FORBIDDEN)
        } else {
            let session = state.get_session(&token).map_err(|err| {
                println!("FromRequest Authenticated read_model db error {}", err);
                StatusCode::UNAUTHORIZED
            })?;

            Ok(AuthenticatedModel(session))
        }
    }
}

impl AuthenticatedModel {
    pub fn into_inner(self) -> ReadModelSession {
        self.0
    }
}
