use anyhow::Result;
use async_graphql::http::{playground_source, GraphQLPlaygroundConfig};
use async_graphql_axum::{GraphQLRequest, GraphQLResponse};
use axum::{
    extract::Extension,
    response::{Html, IntoResponse},
    routing::get,
    Router,
};
use store_write_auth::UserDb;

use crate::{middleware::AuthenticatedUser, schema::AppSchema};

pub async fn serve(schema: AppSchema, user_db: &'static UserDb) -> Result<()> {
    let app = Router::new()
        .route(
            "/write",
            get(graphql_playground).post(graphql_handler),
        )
        .route("/healthz", get(liveness_handler))
        .layer(Extension(schema))
        .layer(Extension(user_db));

    println!("Playground: http://localhost:3000/write");

    axum::Server::bind(&"0.0.0.0:3000".parse()?)
        .serve(app.into_make_service())
        .await?;
    Ok(())
}

async fn graphql_playground() -> impl IntoResponse {
    Html(playground_source(GraphQLPlaygroundConfig::new(
        "/write",
    )))
}

async fn graphql_handler(
    user: AuthenticatedUser,
    schema: Extension<AppSchema>,
    req: GraphQLRequest,
) -> GraphQLResponse {
    schema
        .execute(req.into_inner().data(user.into_inner()))
        .await
        .into()
}

async fn liveness_handler() -> String {
    "OK".to_string()
}
