use anyhow::Result;

use store_write_auth::UserDb;

pub async fn setup() -> Result<&'static UserDb> {
    // Run migrations to setup database
    let db = store_write_auth::init_db()?;

    Ok(db)
}
