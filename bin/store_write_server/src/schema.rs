use async_graphql::{EmptySubscription, Object, Schema};

use store_db::DatabaseImplementation;
use store_write_event_config::EventWriteConfig;
use store_write_gql::Mutation;

pub struct EmptyQuery;

#[Object]
impl EmptyQuery {
    async fn null(&self) -> bool {
        true
    }
}

pub type AppSchema = Schema<EmptyQuery, Mutation, EmptySubscription>;

pub async fn build(db: DatabaseImplementation, event_write_config: EventWriteConfig) -> AppSchema {
    Schema::build(EmptyQuery, Mutation::default(), EmptySubscription)
        .data(db)
        .data(event_write_config)
        .finish()
}
