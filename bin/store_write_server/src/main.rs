use anyhow::Result;
use clap::Parser;
use store_write_event_config::EventWriteConfig;

mod event_db;
mod middleware;
mod schema;
mod server;
mod user_db;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Database url to connect to e.g. `postgresql://postgres:postgres@localhost:5432/events`
    #[arg(
        long,
        default_value = "postgresql://postgres:postgres@localhost:5432/events"
    )]
    database_url: String,
    /// Path to load event write configuration from `./docs/event_write_config_example.yaml`
    #[arg(long, default_value = "./docs/event_write_config_example.yaml")]
    write_config_path: String,
}

#[tokio::main(worker_threads = 12)]
async fn main() -> Result<()> {
    let args = Args::parse();
    let database_url = load_config_value(args.database_url, "DATABASE_URL");

    let write_config_path = load_config_value(args.write_config_path, "WRITE_CONFIG_PATH");
    let write_config_string = std::fs::read_to_string(write_config_path)?;
    let write_config: EventWriteConfig = serde_yaml::from_str(&write_config_string)?;

    println!(
        "Connecting to database: `{}`",
        &database_url.split('@').last().unwrap_or_default()
    );
    println!("Event write config: `{:?}`", write_config);

    let event_db = event_db::setup(&database_url).await?;
    let schema = schema::build(event_db, write_config).await;

    let user_db = user_db::setup().await?;

    server::serve(schema, user_db).await?;
    Ok(())
}

fn load_config_value(arg: String, key: &str) -> String {
    let env_value = std::env::var(key.to_uppercase());
    if let Ok(env_value) = env_value {
        env_value
    } else {
        arg
    }
}
